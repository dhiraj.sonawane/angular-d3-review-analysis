import { Component, ElementRef, Input, OnChanges, ViewChild, ViewEncapsulation, HostListener } from '@angular/core';

import * as d3 from 'd3';
import d3Tip from 'd3-tip';
import { DataModel } from 'src/app/data/data.model';
import {DatePipe} from '@angular/common';

@Component({
selector: 'app-bar-chart',
encapsulation: ViewEncapsulation.None,
templateUrl: './bar-chart.component.html',
styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnChanges {
@ViewChild('chart')
private chartContainer: ElementRef;

private colors: any;

@Input()
data: DataModel[];

margin = {top: 20, right: 20, bottom: 30, left: 40};

constructor() { }

  ngOnChanges(): void {
    if (!this.data) { return; }

    this.createChart();

  }

  onResize() {
    this.createChart();
  }

  private createChart(): void {
    d3.select('svg').remove();

    const element = this.chartContainer.nativeElement;
    const data = this.data;

    const svg = d3.select(element).append('svg')
        .attr('width', element.offsetWidth)
        .attr('height', element.offsetHeight);

    const contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    const contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;


    const x = d3
      .scaleBand()
      .rangeRound([0, contentWidth])
      .padding(0.05)
      .domain(data.map(d => d.ReviewsDate));

    const y = d3
      .scaleLinear()
      .rangeRound([contentHeight, 0])
      .domain([0, d3.max(data, d => d.Reviewsrating)]);

    const g = svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    g.append("text")      // text label for chart Title
               .attr("x", contentWidth/2)
               .attr("y", 0 - (this.margin.top/2))
               .style("text-anchor", "middle")
       	       .style("font-size", "12px")
               .style("text-decoration", "underline")
               .text();

    g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + (contentHeight) + ')')
      .call(d3.axisBottom(x))
      .selectAll("text");

    g.append('g')
      .attr('class', 'axis axis--y')
      .call(d3.axisLeft(y))
      .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.0em')
        .attr('text-anchor', 'end')
        .text('Review Rating');

    const tip = d3Tip();
    tip.attr("class", "d3-tip")
          .html(d => {
            return (
              '<strong>WordCount:</strong> <span style="color:red">' + d.Reviewstext.length + "</span><br><strong>DoRecommend:"
              + d.ReviewsdoRecommend + "</strong>"
)
})

g.call(tip);

    this.colors = d3.scaleLinear().domain([0, this.data.length]).range(<any[]>['red', 'blue']);

    g.append("g")
     .append("text").attr("transform", "rotate(-90)")
     .attr("y", 6).attr("dy", "-5.1em")
     .attr("text-anchor", "end").attr("font-size", "12px")
     .attr("stroke", "blue").text("Review Rating");

    g.selectAll('.bar')
      .data(data)
      .enter().append('rect')
        .attr('class', 'bar')
        .attr('x', d => x(d.ReviewsDate))
        .attr('y', d => y(d.Reviewsrating))
        .attr('width', x.bandwidth())
        .attr('height', d => contentHeight - y(d.Reviewsrating))
        .style('fill', (d, i) => this.colors(i))
        .on("mouseover", (d, i, n) => tip.show(d, n[i]))
        .on("mouseout", d => tip.hide(d));
  }
}
