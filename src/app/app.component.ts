import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { DataModel } from 'src/app/data/data.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  data: Observable<DataModel>;

  products = ['All-New Fire HD 8 Tablet, 8 HD Display, Wi-Fi, 16 GB - Includes Special Offers, Magenta',
              'Fire Tablet, 7 Display, Wi-Fi, 8 GB - Includes Special Offers, Magenta']

  constructor(private http: HttpClient) {
    this.data = this.http.get<DataModel>('./assets/data.json');
  }

  changeproducts(event) {
          console.log(event);
          let selectedValue = event.target.value;
          if(selectedValue == 'All-New Fire HD 8 Tablet, 8 HD Display, Wi-Fi, 16 GB - Includes Special Offers, Magenta'){
              this.data = this.http.get<DataModel>('./assets/data.json');
          }else if(selectedValue == 'Fire Tablet, 7 Display, Wi-Fi, 8 GB - Includes Special Offers, Magenta'){
              this.data =this.http.get<DataModel>('./assets/data2.json');
          }
   }

}
