export interface DataModel {
    Product: string;
    ReviewsDate: string;
    ReviewsdoRecommend: string;
    Reviewsrating: number;
    Reviewstext: string;
    Reviewstitle: string;
}
